#!/bin/bash
set -e -x

wget https://bitbucket.org/testobject/org.testobject.aws/src/master/android/android-sdk_r16-linux.tgz
tar xvzf android-sdk_r16-linux.tgz
cd android-sdk-linux/tools/
./android update sdk --no-ui --filter platform